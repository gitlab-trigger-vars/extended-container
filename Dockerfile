FROM registry.gitlab.com/gitlab-trigger-vars/base-container

ADD [ "*.sh", "/usr/local/bin" ]

ENTRYPOINT [ "entrypoint.sh" ]
